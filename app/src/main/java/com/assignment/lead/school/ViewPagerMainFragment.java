package com.assignment.lead.school;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created on 09-01-2019.
 */
public class ViewPagerMainFragment extends Fragment {

    final static String LAYOUT_ID = "layoutid";

    public static ViewPagerMainFragment newInstance(int layoutId) {
        ViewPagerMainFragment pane = new ViewPagerMainFragment();
        Bundle args = new Bundle();
        args.putInt(LAYOUT_ID, layoutId);
        pane.setArguments(args);
        return pane;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (ViewGroup) inflater.inflate(getArguments().getInt(LAYOUT_ID, -1), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (view.findViewById(R.id.txtContinue) != null) {
            view.findViewById(R.id.txtContinue).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DashboardActivity.start(view.getContext());
                }
            });
        }

    }
}
