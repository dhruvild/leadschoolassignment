package com.assignment.lead.school;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment.lead.school.databinding.ActivityIntroBinding;

public class MainActivity extends AppCompatActivity {

    private static final int NUM_PAGES = 3;
    private ActivityIntroBinding B;
    private PagerAdapter mPagerAdapter;
    private Animation animFadeIn, animFadeOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        B = DataBindingUtil.setContentView(this, R.layout.activity_intro);

        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);

        animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_out);

        B.txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadDashboardActivity();
            }
        });

        B.viewPager.setAdapter(mPagerAdapter);
        B.viewPager.setPageTransformer(true, new PageTransformer());

        B.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setIndicator(position);
                if (position == (NUM_PAGES - 1)) {
                    B.txtSkip.setVisibility(View.GONE);
                    // start fade out animation
                    B.txtSkip.startAnimation(animFadeOut);
                    B.viewpagerIndicator.setVisibility(View.GONE);
                } else {
                    B.viewpagerIndicator.setVisibility(View.VISIBLE);
                    if (B.txtSkip.getVisibility() == View.GONE) {
                        B.txtSkip.setVisibility(View.VISIBLE);
                        // start fade in animation
                        B.txtSkip.startAnimation(animFadeIn);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        buildCircles();
    }

    private void loadDashboardActivity() {
        DashboardActivity.start(this);
    }

    private void buildCircles() {
        float scale = getResources().getDisplayMetrics().density;
        int padding = (int) (scale / 3);

        for (int i = 0; i < NUM_PAGES; i++) {
            ImageView circle = new ImageView(this);
            circle.setImageResource(R.drawable.ic_swipe_indicator_white_18dp);
            circle.setLayoutParams(new ViewGroup.LayoutParams(40, 40));
            circle.setAdjustViewBounds(true);
            circle.setPadding(padding, 0, padding, 0);
            B.viewpagerIndicator.addView(circle);
        }

        setIndicator(0);
    }

    private void setIndicator(int index) {
        if (index < NUM_PAGES) {
            for (int i = 0; i < NUM_PAGES; i++) {
                ImageView circle = (ImageView) B.viewpagerIndicator.getChildAt(i);
                if (i == index) {
                    circle.setColorFilter(getResources().getColor(R.color.colorAccent));
                } else {
                    circle.setColorFilter(getResources().getColor(android.R.color.darker_gray));
                }
            }
        }
    }

    public void animateTextview() {
        int[] textViewIds = {R.id.tv1, R.id.tv2, R.id.tv3,};
        int i = 1;

        for (int viewId : textViewIds) {
            Animation fadeAnimation = AnimationUtils.loadAnimation(this, R.anim.fading_effect);
            fadeAnimation.setStartOffset(i * 200);
            TextView textView = (TextView) findViewById(viewId);
            textView.startAnimation(fadeAnimation);
            i++;
        }
    }

    private void changeViewBgColor(View view, int color) {
        DrawableCompat.setTint(view.getBackground(), ContextCompat.getColor(view.getContext(), color));
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ViewPagerMainFragment tp = null;
            switch (position) {
                case 0:
                    tp = ViewPagerMainFragment.newInstance(R.layout.fragment_slide_1);
                    break;
                case 1:
                    tp = ViewPagerMainFragment.newInstance(R.layout.fragment_slide_2);
                    break;
                case 2:
                    tp = ViewPagerMainFragment.newInstance(R.layout.fragment_slide_3);
                    break;
            }

            return tp;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public class PageTransformer implements ViewPager.PageTransformer {

        @Override
        public void transformPage(View page, float position) {

            View imgMusic = page.findViewById(R.id.imgMusic);
            View imgChart = page.findViewById(R.id.imgChart);
            View imgColorbox = page.findViewById(R.id.imgColorbox);
            View imgVideo = page.findViewById(R.id.imgVideo);
            View imgPie = page.findViewById(R.id.imgPie);
            View imgMap = page.findViewById(R.id.imgMap);

            View text = page.findViewById(R.id.img);
            View llView = page.findViewById(R.id.llView);

            View viewOne = page.findViewById(R.id.viewOne);
            View viewTwo = page.findViewById(R.id.viewTwo);
            View viewThree = page.findViewById(R.id.viewThree);
            View viewFour = page.findViewById(R.id.viewFour);
            View viewFive = page.findViewById(R.id.viewFive);
            View viewSix = page.findViewById(R.id.viewSix);

            int pageWidth = (int) page.getWidth();
            float pageWidthTimesPosition = pageWidth * position;
            float absPosition = Math.abs(position);

            if (position < -1) {
                B.viewpagerIndicator.setVisibility(View.GONE);
                // [-00,-1): the page is way off-screen to the left.
            }

            if (position <= -1.0f || position >= 1.0f) {
                // Page is not visible -- stop any running animations
            } else if (position == 0.0f) {
                // Page is selected -- reset any views if necessary
            } else {

                //First Slide Animation
                if (imgMusic != null) {
                    imgChart.setTranslationX(pageWidthTimesPosition * 0.5f);
                }

                if (imgChart != null) {
                    imgChart.setTranslationX(pageWidthTimesPosition * 0.5f);
                }

                if (imgColorbox != null) {
                    imgColorbox.setTranslationX(pageWidthTimesPosition * 2.5f);
                }

                if (imgMap != null) {
                    imgMap.setTranslationX(pageWidthTimesPosition * 1.2f);
                }

                if (imgVideo != null) {
                    imgVideo.setTranslationX(pageWidthTimesPosition * 0.2f);
                }

                if (imgPie != null) {
                    imgPie.setTranslationX(pageWidthTimesPosition * 0.7f);
                }

                //Second Slide Animation
                if (text != null) {
                    text.setRotation(pageWidthTimesPosition > 0 ? -Math.abs(pageWidthTimesPosition / 3) : Math.abs(pageWidthTimesPosition / 3));
                    animateTextview();
                }

                if (llView != null) {
                    llView.setScaleX(1.0f - absPosition * 2);
                    llView.setScaleY(1.0f - absPosition * 2);
                    llView.setAlpha(1.0f - absPosition * 2);
                }

                //Third Slide Animation
                if (viewOne != null) {
                    changeViewBgColor(viewOne, R.color.green);
                }

                if (viewTwo != null) {
                    changeViewBgColor(viewTwo, R.color.indigo);
                    viewTwo.setTranslationX(pageWidthTimesPosition * 1.0f);
                }

                if (viewThree != null) {
                    changeViewBgColor(viewThree, R.color.light_blue);
                    viewThree.setTranslationX(pageWidthTimesPosition * 1.0f);
                }

                if (viewFour != null) {
                    changeViewBgColor(viewFour, R.color.red);
                    viewFour.setTranslationX(pageWidthTimesPosition * 1.0f);
                }

                if (viewFive != null) {
                    changeViewBgColor(viewFive, R.color.yellow);
                }

                if (viewSix != null) {
                    changeViewBgColor(viewSix, R.color.deep_orange);
                }

            }
        }
    }
}